package ivs.ignis;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class CalculatorFX extends Application {

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("Calculator.fxml"));
        Parent     root   = loader.load();
        Scene      scene  = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("IVS Ignis Calculator");
        stage.getIcons().add(new Image("images/icon.ico")); // FEATURE: Change icon
        stage.setResizable(false); // TODO: responsive
        stage.show();
    }
}
