package ivs.ignis.tokenizing;

import java.util.Objects;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
public final class Token {

    private TokenType type;

    private String value;

    public Token(TokenType type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public TokenType getType() {
        return type;
    }

    public boolean isType(TokenType type) {
        return getType() == type;
    }

    @Override
    public String toString() {
        return "Token{" +
               "type=" + type +
               ", value='" + value + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        return Objects.equals(this.type, token.type) &&
               Objects.equals(this.value, token.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.type, this.value);
    }
}
