package ivs.ignis.tokenizing;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
public class UnrecognizedSequenceException extends RuntimeException {

    public UnrecognizedSequenceException() {
        super();
    }

    public UnrecognizedSequenceException(String message) {
        super(message);
    }

    public UnrecognizedSequenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnrecognizedSequenceException(Throwable cause) {
        super(cause);
    }

    protected UnrecognizedSequenceException(String message, Throwable cause, boolean enableSuppression,
                                            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
