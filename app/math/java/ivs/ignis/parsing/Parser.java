package ivs.ignis.parsing;

import ivs.ignis.tokenizing.Token;
import ivs.ignis.tokenizing.TokenType;
import ivs.ignis.tokenizing.Tokenizer;
import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Objects;

import static ivs.ignis.tokenizing.TokenType.*;

/**
 * @author Vojtěch Bargl <bargl.vojtech@gmail.com>
 */
public class Parser {

    private static long PRECISION = 31;

    private Tokenizer tokenizer;

    public Parser(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    /**
     * Replace active tokenizer with specified {@link Tokenizer}.
     *
     * @param tokenizer - to be replace with.
     */
    public void setTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public Apfloat parse() {
        Apfloat expression = parseExpression();
        if (!checkToken(EOF)) throw new SyntaxException("Not end!");
        return expression;
    }

    /**
     * Parse next token sequence as: <term>((PLUS|MINUS)<term>)*
     *
     * @return Apfloat value of sequence.
     */
    Apfloat parseExpression() {
        Apfloat res = parseTerm();

        while (checkToken(PLUS, MINUS)) {
            Token   operator = stepToken(PLUS, MINUS);
            Apfloat operand  = parseTerm();

            res = operator.isType(PLUS) ? res.add(operand) : res.subtract(operand);

            res.precision(PRECISION);
        }

        return res;
    }

    /**
     * Parse next token sequence as: <unary>((MULTIPLY|DIVIDE)<unary>)*
     *
     * @return Apfloat value of sequence.
     */
    Apfloat parseTerm() {
        Apfloat res = parseUnary();

        while (checkToken(MULTIPLY, DIVIDE)) {
            Token   operator = stepToken(MULTIPLY, DIVIDE);
            Apfloat operand  = parseUnary();

            res = operator.isType(MULTIPLY) ? res.multiply(operand) : res.divide(operand);

            res = ApfloatMath.round(res, PRECISION - 1, RoundingMode.HALF_UP);
            res.precision(PRECISION);
        }

        return res;
    }

    /**
     * Parse next token sequence as: (+|-)* <exponent>
     *
     * @return Apfloat value of token sequence.
     */
    Apfloat parseUnary() {
        boolean sign = true; // PLUS as default
        while (checkToken(PLUS, MINUS)) sign = stepToken(PLUS, MINUS).isType(MINUS) != sign;

        Apfloat apfloat = parseExponent();
        return sign ? apfloat : apfloat.negate();
    }

    /**
     * Parse next token sequence as: <factor>((POWER)<unary>)?
     *
     * @return Aplfloat value of sequence.
     */
    Apfloat parseExponent() {
        Apfloat decimal = parseFactor();

        if (checkToken(POWER)) {
            stepToken(POWER);
            Apfloat exponent = parseUnary();
            decimal = ApfloatMath.pow(decimal, exponent);
        }

        return decimal;
    }

    /**
     * Parse next token sequence as: INTEGER|DOUBLE|'('<expr>')'|<factor>'!'
     *
     * @return Apfloat value of token sequence.
     */
    Apfloat parseFactor() {
        Apfloat number = null;

        if (checkToken(INTEGER, DOUBLE)) {
            Token token = stepToken(INTEGER, DOUBLE);
            number = new Apfloat(token.getValue(), PRECISION);
        } else if (checkToken(OPEN_PARENTHESIS)) {
            stepToken(OPEN_PARENTHESIS);
            number = parseExpression();
            stepToken(CLOSED_PARENTHESIS);
        }

        while (checkToken(FACTORIAL)) {
            if (!isInteger(number)) {
                // TODO: Gamma function for double value
                throw new IllegalArgumentException("Factorial of double value!");
            }

            number = MathHelper.factorial(number, PRECISION);
            stepToken(FACTORIAL);
        }

        if (number == null) {
            throw new UnknownTokenException();
        }

        return number;
    }

    /**
     * @param types set of types that could be stepped.
     *
     * @return Token if its type is in types.
     */
    private Token stepToken(TokenType... types) {
        Token token = tokenizer.current();

        for (TokenType type : types) {
            if (token.isType(type)) return tokenizer.gain();
        }

        throw new UnknownTokenException(
                String.format("Expected any of %s, but got: %s!", Arrays.toString(types), token)
        );
    }

    /**
     * @param types set of types that are allowed.
     *
     * @return True if any is find, false otherwise.
     */
    private boolean checkToken(TokenType... types) {
        Token token = tokenizer.current();

        for (TokenType type : types) {
            if (token.isType(type)) return true;
        }

        return false;
    }

    private boolean isInteger(Apfloat apfloat) {
        return apfloat != null && Objects.equals(apfloat.truncate(), apfloat);
    }
}
